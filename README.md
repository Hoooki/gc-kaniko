# GitLab CI template for Kaniko

This projects is a Gitlab [CI/CD Components](https://docs.gitlab.com/ee/ci/components/) that's implements [Kaniko](https://github.com/GoogleContainerTools/kaniko). 

## Usage

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  - component: gitlab.com/hoooki/gc-kaniko@0.1.1
```

## Configuration

Thanks to the Gitlab `inputs` system you can configure your implemented Components. 

### Parameters

---

`kanikoStage` : Stage where Kaniko job is executed.

Default : `build`

---

`dockerfile` : Path to your targeted Dockerfile.

Default : `./Dockerfile`

---

`context` : Docker build execution context.

Default : `.`

---

`buildArgs` : Arguments used during the build phase of your image. 

Default : `''`

Format : `'--build-arg MY_ARG=my_value --build-arg MY_SND_ARG=my_snd_value'`

---

`target` : 	Set the target build stage to build.

Default : `''`

---

`publish` : Publish your image to the project repository.

Default : `'true'`

---

`exposeTarget` : Specify if you want to add target as tag's suffix. Example: repository/my-image:1.0.0-target

Default : `'false'`

---

`name` : Image's name

Default : `''`

*If value is empty, kaniko will use your project name as value*


---

`name` : Image's tag

Default : `''`

*If value is empty, kaniko will use the `$CI_COMMIT_REF_SLUG`