# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.2] - 2024-02-24

### BugFixes

- Used own Regex to format image's tag instead of `$CI_COMMIT_REF_SLUG`

## [0.1.1] - 2024-02-14

### BugFixes

- Remove Kaniko 'workflow:rules' that blocked release

## [0.1.0] - 2024-02-14

### Features

- Created Kaniko CI job as Component